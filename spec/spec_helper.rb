require "bundler/setup"
require "registeration"


RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  # Capybara config here
  Capybara.configure do |capybara|
    # Don't run a rack app
    capybara.run_server = false

    # Define your app host here
    capybara.app_host = 'http://www.google.com'

    # I'm using the mechanize driver but your free to use your favorite one (env-js, selenium, ...)
    capybara.default_driver = :selenium
  end


  # Don't forget to tell to RSpec to include Capybara :)z
  config.include Capybara::DSL

  CUCUMBER_PUBLISH_QUIET=true
end

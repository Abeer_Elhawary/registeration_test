gem 'activerecord'
gem 'activerecord-oracle_enhanced-adapter'

begin
  require 'rspec/expectations';
rescue LoadError;
  require 'spec/expectations';
end

require 'active_record'
require 'byebug'
require 'capybara'
require 'capybara/dsl'
require 'capybara/cucumber'
require 'capybara/rspec'
require 'rspec'
require 'capybara-screenshot/cucumber'
require 'csv'
require 'factory_girl'
require 'json'
require 'launchy'
require 'roo'
require 'ruby-plsql'
require 'rubygems'
require 'selenium/webdriver'
require 'socket'
require 'wannabe_bool'
require './spec/spec_helper.rb'


Capybara.register_driver :chrome do |app|
  # Set up options
  options = Selenium::WebDriver::Chrome::Options.new
  client = Selenium::WebDriver::Remote::Http::Default.new
  client.read_timeout = 120

  # Start options
  options.add_argument('--disable-print-preview')
  options.add_argument('--ignore-certificate-errors')
  options.add_argument('--disable-popup-blocking')
  options.add_argument('--disable-translate')
  # options.add_argument('--headless') if headless
  options.add_argument('--window-size=1920,1080')
  options.add_argument('--start-maximized')
  Capybara::Selenium::Driver.new(app, :browser => :chrome, options: options, http_client: client)
end
Capybara.default_driver = :chrome

$url = 'https://www.phptravels.net/'

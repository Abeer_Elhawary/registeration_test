# Created by Abeer-Sayed  11th Oct 2020

Feature: Verify that the registeration page is working correctly.

  Background:  Open the URL of the web-page and Navigate to registerarion page to apply the scenarios
    Given I open the home page
    And I click the "My Account" link
    And I click the "Sign Up" link
    And I wait 3 seconds

  Scenario: the registered user can log into the system correctly
    When I have entered valid value into the "First Name" field
    And I have entered valid value into the "Last Name" field
    And I have entered valid value into the "Email" field
    And I have entered valid value into the "Mobile Number" field
    And I have entered valid value into the "Password" field
    And I have entered valid value into the "Confirm Password" field
    And I click the "Sign Up" Button
    And I wait 3 seconds
    Then I setup a new user
    Given I log out
    And I have entered registered "Email"
    And I have entered registered "Password"
    And I click the "Login" Button
    And I wait 3 seconds
    Then I should see "MY PROFILE"

  Scenario: Test that I should see an error message when I register by already registered mail
    When I have entered valid value into the "First Name" field
    And I have entered valid value into the "Last Name" field
    And I have entered valid value into the "Email" field
    And I have entered valid value into the "Mobile Number" field
    And I have entered valid value into the "Password" field
    And I have entered valid value into the "Confirm Password" field
    And I click the "Sign Up" Button
    And I wait 3 seconds
    Then I setup a new user
    Given I log out
    And I click the "My Account" link
    And I click the "Sign Up" link
    And I wait 3 seconds
    And I have entered registered "Email"
    When I have entered valid value into the "First Name" field
    And I have entered valid value into the "Last Name" field
    And I have entered valid value into the "Mobile Number" field
    And I have entered valid value into the "Password" field
    And I have entered valid value into the "Confirm Password" field
    And I click the "Sign Up" Button
    And I wait 3 seconds
    Then I should see "Email Already Exists"

  Scenario Outline: Test Different error Messages on the registeration form
    When I have entered "<Fname>" into the "First Name" field
    When I have entered "<Lname>" into the "Last Name" field
    When I have entered "<Email>" into the "Email" field
    When I have entered "<Mobile>" into the "Mobile Number" field
    When I have entered "<Password>" into the "Password" field
    When I have entered "<ConfirmPassword>" into the "Confirm Password" field
    And I click the "Sign Up" Button
    Then I should see "<Error_Message>"

    Examples:
      | Fname | Lname | Email    | Mobile      | Password  | ConfirmPassword | Error_Message                                               |
      | SQA   | Test  | test.com | 01112345678 | Test_1234 | Test_1234       | The Email field must contain a valid email address.         |
      | SQA   | Test  | test.com | 01112345678 | Test      | Test_1234       | The Password field must be at least 6 characters in length. |

require 'faker'

#######################################################
### Methods
#######################################################

#  Generate valid passsword with a customized length
def random_password(length)
  password = ('0'..'9').to_a + ('A'..'Z').to_a + ('a'..'z').to_a
  password.sort_by {rand}.join[0...length]
end


# Created to Generate random valid data for each required field
def random_valid_value (field)

  if field.eql? 'Email'
    @email = "#{Faker::Lorem.words(2).join('.')}@example.com"

  elsif field.eql? 'First Name'
    @first_name = "#{Faker::Name.first_name}".sub(/^./, &:upcase)

  elsif field.eql? 'Last Name'
    @last_name = "#{Faker::Name.last_name}".sub(/^./, &:upcase)

  elsif field.eql? 'Mobile Number'
    @mobile_number = "#{Faker::PhoneNumber.phone_number}"

  elsif field.eql? 'Password'
    @password = random_password 10

  elsif field.eql? 'Confirm Password'
    @password

  end
end


#########################################################
### STEP_Definitions
#########################################################

Given(/^I open the home page$/) do
  visit $url
end


Given(/^I do nothing$/) do
end

And(/^I click the "([^"]*)" link$/) do |link|
  page.first(:xpath, "//a[contains(.,'#{link}')]").click
end

When /^[I ]*wait [for ]*(\d+) seconds?$/ do |seconds|
  sleep(seconds.to_i)
end

When(/^[I ]*have entered "([^"]*)" into the "([^"]*)" field/) do |value, field|
  fill_in field, :with => value
end

And(/^I have entered valid value into the "([^"]*)" field$/) do |field|
  value = random_valid_value (field)
  # puts "#{value}"
  fill_in field, :with => value
end

And(/^I click the "([^"]*)" Button$/) do |button|
  click_button button
end


Then(/^[I ]*(should|should not) see "([^"]*)"$/) do |vision, text|

  expect(page.has_content?(text)).to eq(vision == 'should')

end

Then(/^I setup a new user$/) do
  @user = User.new(@first_name, @last_name, @mobil, @email, @password)
end

And(/^I have entered registered "([^"]*)"$/) do |field|
  fill_in field, :with => @user.get_email if field.eql? 'Email'
  fill_in field, :with => @user.get_password if field.eql? 'Password'

end

Given(/^I log out$/) do
  step %{I click the "#{@user.get_firstname}" link}
  step %{I click the "Logout" link}
end
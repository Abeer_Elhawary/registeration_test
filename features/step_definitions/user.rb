# User Calss contains all User attributes
class User
  def initialize(first_name, last_name, mobile_number, email, password)
    @first_name = first_name
    @last_name = last_name
    @mobile_number = mobile_number
    @email = email
    @password = password
  end

  def get_email
    @email
  end

  def get_password
    @password
  end

  def get_firstname
    @first_name
  end
end